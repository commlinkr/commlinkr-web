# Commlinkr Web

## /client

## /server

* `server.js` will run on port 3000, and allows calls in from port 8000 on localhost.
* Executing `npm start` includes pre-start execution of `npm install`
* Authenticaion string is provided via `authConfig.json`, which is not included in this repository (for both security and portability reasons)

### endpoints:

*runner*:

* `GET` : `/api/runners`: returns a full list of runners
* `GET` : `/api/runners/:id`: returns a specific runner by Mongo defined id
* `POST` : `/api/runner/edit` updates an existing runner by Mongo defined id
* `POST` : `/api/runner/new/`: inserts a new runner record

*weapons*:

* `GET` : `/api/weapons/`: returns a full list of all weapons
* `GET` : `/api/weapon/:id`: returns a specific ewapon by Mongo defined id
* `GET` : `/api/weapons/category/:category`: returns a subset of weapons by defined category

### dependencies:

* express
* body-parser
* cors
* mongodb (MongoClient and ObjectId specifically)

#
[New contributor documentation](https://docs.google.com/document/d/e/2PACX-1vR-q4fZUJuU5EV2Hx3vZWPT9bMLafEb0eTNuUa_kSq0PnvDXawhGnBgn4p3zEzxh4q_afs2fXbsOpYI/pub)