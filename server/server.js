const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const MongoClient = require('mongodb').MongoClient;
const authConfig = require('./authConfig.json');
const app = express();
const PORT = 3000;
const { mongoConnectionUri } = require('./authConfig.json');

var ObjectId = require('mongodb').ObjectId;
var corsOptions = {
	origin: 'http://localhost:8000'
};

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);
});

app.route('/api/runners').get((req, res) => {
	MongoClient.connect(mongoConnectionUri, function(err, client) {
		if (err) console.log(err);

		var db = client.db('runner');
		db.collection('runners').find().toArray(function(err, result) {
			if (err) throw err;
			res.send(result);
		});
	});
});

app.route('/api/runners/:id').get((req, res) => {
	const requestedRunnerId = req.params['id'];
	MongoClient.connect(authConfig.mongoConnectionUri, function(err, client) {
		if (err) console.log(err);
		var db = client.db('runner');
		var query = { _id: ObjectId(requestedRunnerId) };
		var runnerData = {};
		db.collection('runners').find(query).toArray(function(err, result) {
			if (err) throw err;

			runnerData = result;
			if (runnerData[0].weapons) {
				var runnerWeaponIds = runnerData[0].weapons.map((weapon) => weapon.oid);
				db.collection('weapons').find({ _id: { $in: runnerWeaponIds } }).toArray(function(err, result) {
					if (err) {
						console.log('Error! ' + err);
						throw err;
					}
					runnerData.weaponData = result;
					res.send(runnerData);
				});
			} else {
				res.send(runnerData);
			}
		});
	});
});

app.route('/api/runner/edit').post((req, res) => {
	MongoClient.connect(authConfig.mongoConnectionUri, function(err, client) {
		if (err) console.log(err);

		var db = client.db('runner');
		var requestedRunnerId = ObjectId(req.body.id);
		var addedWeaponIds = req.body.addedWeapons;
		addedWeaponIds.forEach((value) => {
			var ref = {
				$ref: 'weapons',
				$id: ObjectId(value),
				$db: 'runner'
			};
			req.body.runnerData.weapons.push(ref);
		});

		var addedSpellIds = req.body.addedSpells;
		addedSpellIds.forEach((value) => {
			var ref = {
				$ref: 'spells',
				$id: ObjectId(value),
				$db: 'runner'
			};
			req.body.runnerData.spells.push(ref);
		});

		var query = { _id: ObjectId(requestedRunnerId) };

		db
			.collection('runners')
			.findOneAndUpdate(
				query,
				{
					$set: req.body.runnerData
				},
				{
					upsert: false
				}
			)
			.then((result) => {
				res.send(result);
			})
			.catch((error) => console.error(error))
			.then((result) => {
				res.send(result);
			});
	});
});

app.route('/api/initiative/new').post((req, res) => {
	MongoClient.connect(authConfig.mongoConnectionUri, function(err, client) {
		if (err) console.log(err);

		var db = client.db('runner');

		const query = {
			'character.user.name': req.body.character.user.name,
			'character.name': req.body.character.name,
			scene_id: ObjectId(req.body.scene_id)
		};

		const update = {
			$set: {
				character: {
					user: {
						name: req.body.character.user.name
					},
					name: req.body.character.name
				},
				initiative_score: req.body.initiative_score,
				scene_id: ObjectId(req.body.scene_id)
			}
		};
		var options = {
			upsert: true
		};

		db
			.collection('initiative')
			.updateOne(query, update, options)
			.then((result) => {
				res.send(result);
			})
			.catch((error) => res.send(result))
			.then((result) => {
				res.send(result);
			});
	});
});

app.route('/api/initiatives/:scene').get((req, res) => {
	MongoClient.connect(mongoConnectionUri, function(err, client) {
		if (err) console.log(err);
		var db = client.db('runner');
		const scene = req.params['scene'];
		var query = { scene_id: ObjectId(scene) };
		db.collection('initiative').find(query).sort({ initiative_score: -1 }).toArray(function(err, result) {
			if (err) throw err;
			res.send(result);
		});
	});
});

app.route('/api/runner/new/').post((req, res) => {
	MongoClient.connect(authConfig.mongoConnectionUri, function(err, client) {
		if (err) console.log(err);

		var db = client.db('runner');

		db
			.collection('runners')
			.insertOne(req.body)
			.then((result) => {
				res.send(result);
			})
			.catch((error) => res.send(result))
			.then((result) => {
				res.send(result);
			});
	});
});

app.route('/api/weapons/').get((req, res) => {
	MongoClient.connect(mongoConnectionUri, function(err, client) {
		if (err) console.log(err);
		var db = client.db('runner');
		db.collection('weapons').find().toArray(function(err, result) {
			if (err) throw err;
			res.send(result);
		});
	});
});

app.route('/api/weapons/').post((req, res) => {
	MongoClient.connect(mongoConnectionUri, function(err, client) {
		if (err) console.log(err);
		var db = client.db('runner');

		var weaponOIds = [];
		req.body.weaponIds.forEach(function(value) {
			weaponOIds.push(ObjectId(value));
		});

		var weaponData = [];
		db.collection('weapons').find({ _id: { $in: weaponOIds } }).toArray(function(err, result) {
			if (err) {
				console.log('Error! ' + err);
				throw err;
			}
			weaponData = result;
			res.send(weaponData);
		});
	});
});

app.route('/api/spells/category/:category').get((req, res) => {
	MongoClient.connect(mongoConnectionUri, function(err, client) {
		if (err) console.log(err);
		var db = client.db('runner');
		const category = req.params['category'];
		var query = { category: category };
		db.collection('spells').find(query).toArray(function(err, result) {
			if (err) throw err;
			res.send(result);
		});
	});
});

app.route('/api/spells/').get((req, res) => {
	MongoClient.connect(mongoConnectionUri, function(err, client) {
		if (err) console.log(err);
		var db = client.db('runner');
		db.collection('spells').find().toArray(function(err, result) {
			if (err) throw err;
			res.send(result);
		});
	});
});

app.route('/api/spells/').post((req, res) => {
	console.log('/api/spells called.');

	MongoClient.connect(mongoConnectionUri, function(err, client) {
		if (err) console.log(err);
		var db = client.db('runner');

		var spellOIds = [];
		req.body.spellIds.forEach(function(value) {
			spellOIds.push(ObjectId(value));
		});

		var spellData = [];
		db.collection('spells').find({ _id: { $in: spellOIds } }).toArray(function(err, result) {
			console.log('get spellData');
			if (err) {
				console.log('Error! ' + err);
				throw err;
			}
			spellData = result;
			res.send(spellData);
		});
	});
});

app.route('/api/spells/category/:category').get((req, res) => {
	MongoClient.connect(mongoConnectionUri, function(err, client) {
		if (err) console.log(err);
		var db = client.db('runner');
		const category = req.params['category'];
		var query = { category: category };
		db.collection('spells').find(query).toArray(function(err, result) {
			if (err) throw err;
			res.send(result);
		});
	});
});

app.route('/api/runner/delete/').post((req, res) => {
	MongoClient.connect(authConfig.mongoConnectionUri, function(err, client) {
		if (err) console.log(err);

		var db = client.db('runner');
		var requestedRunnerId = ObjectId(req.body.id);
		var data = { _id: requestedRunnerId };
		db
			.collection('runners')
			.deleteOne(data)
			.then((result) => {
				res.send(result);
			})
			.catch((error) => res.send(error));
	});
});
