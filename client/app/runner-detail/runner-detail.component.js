'use strict';

angular.module('runnerDetail').component('runnerDetail', {
	templateUrl: 'runner-detail/runner-detail.template.html',
	controller: [
		'$http',
		'$routeParams',
		'$scope',
		function RunnerDetailController($http, $routeParams, $scope) {
			$scope.runnerId = $routeParams.runnerId;
			$scope.runnerData = {};
			$scope.weaponData = [];
			this.runnerDataUri = 'api/runners/' + $scope.runnerId;
			$http.get(this.runnerDataUri).then(function(response) {
				$scope.runnerData = response.data[0];
				if ($scope.runnerData.weapons) {
					var weaponIds = $scope.runnerData.weapons.map((weapon) => weapon.$id);
					var data = {};
					data.weaponIds = weaponIds;
					$http.post('api/weapons', data).then(function(response) {
						$scope.weaponData = response.data;
					});
				}
			});

			$scope.deleteRunner = function() {
				if(confirm("Are you sure to delete " + $scope.runnerData.Name)) {
					var data = {
						id: $scope.runnerId
					};

					$http.post('api/runner/delete', data).then(function(response) {
						window.location.href = '/#!/roster';
					});
				}
			};
		}
	]
});
