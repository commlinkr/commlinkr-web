'use strict';

angular.module('initiative').component('initiative', {
	templateUrl: 'initiative/initiative.template.html',
	controller: [
		'$http',
		'$scope',
		function InitiativeController($http, $scope) {
			$scope.playerName = 'Sex Bobomb';
			$scope.runnerName = 'Narc Man';
			$scope.dicePool = 1;
			$scope.modifier = 0;
			$scope.initiative = 0;
			$scope.sceneId = '6000f81452b5e0d3a20e3b29';

			$scope.saveInitiative = function() {
				var roll = 0;
				for (let i = 0; i < $scope.dicePool; i++) {
					roll += Math.floor(Math.random() * Math.floor(6)) + 1;
				}

				$scope.initiative = roll + parseInt($scope.modifier, 10);

				var initiativeDoc = {
					character: {
						user: {
							name: $scope.playerName
						},
						name: $scope.runnerName
					},
					initiative_score: $scope.initiative,
					scene_id: $scope.sceneId
				};

				$http.post('api/initiative/new', initiativeDoc).then(function(response) {
					console.log(response);
				});
			};
		}
	]
});
