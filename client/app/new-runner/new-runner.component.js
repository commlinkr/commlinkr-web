'use strict';

angular.module('newRunner').component('newRunner', {
	templateUrl: 'new-runner/new-runner.template.html',
	controller: [
		'$http',
		'$routeParams',
		'$scope',
		function NewRunnerController($http, $routeParams, $scope) {
			/**
			 * We create a new scope variable called `newRunner` and initialize 
			 * it as an empty object.  This is so when the front end first looks
			 * for the `newRunner` object to pull data from, it finds an empty
			 * object rather than nothing.  The actual data will be populated
			 * when we use an `$http.get` callout to pull in the blank character
			 * JSON that's in `data-template/`.
			 */
			$scope.newRunner = {};
			/**
			 * Here we are making an $http.get callout to load the JSON that is 
			 * stored in the data-template directory.
			 */
			$http.get('/data-template/new-runner.json').then(function(response) {
				$scope.newRunner = response.data;
			});

			/**
			 * The `saveRunner` function is called in the HTML template when the user
			 * clicks the "Save" button.
			 */
			$scope.saveRunner = function() {
				/**
				 * Since we're inserting new data to the database (rather than just retrieving
				 * it), we use `$http.post` instead of `$http.get`. The first difference here is
				 * that a GET callout will look like this:
				 * 			
				 * 		http.get('url/:param').then(function(....))
				 * 
				 * While a POST callout will be structured like this:
				 * 
				 * 		http.get('url', paramJson).then(function(...))
				 * 
				 * In other words, GET passes information by adding it to the URL (e.g. ":param"),
				 * while POST instead has a value that follows after the URL is defined (e.g. "paramJson").
				 */
				$http.post('api/runner/new', $scope.newRunner).then(function(response) {
					/**
					 * We will probably add some additional code here in the future, but for now,
					 * I just wanted to print the "response" that's returned from the server to the
					 * console.
					 */
					console.log(response);
				});
			};
		}
	]
});
