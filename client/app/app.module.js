'use strict';

angular.module('commlinkrApp', [ 'ngRoute', 'runnerDetail', 'runnersList', 'newRunner', 'editRunner' ]);