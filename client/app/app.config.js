'use strict';
angular.module('commlinkrApp').config([
	'$routeProvider',
	function config($routeProvider) {
			$routeProvider
				.when('/roster', {
					template: '<runners-list></runners-list>'
				})
				.when('/runner/:runnerId', {
					template: '<runner-detail></runner-detail>'
				})
				.when('/newrunner', {
					template: '<new-runner></new-runner>'
				})
				.when('/edit/:runnerId', {
					template: '<edit-runner></edit-runner>'
				})
				.otherwise('/roster');
		}
]);
