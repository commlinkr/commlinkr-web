'use strict';

angular.module('editRunner').component('editRunner', {
	templateUrl: 'edit-runner/edit-runner.template.html',
	controller: [
		'$http',
		'$routeParams',
		'$scope',
		function EditRunnerController($http, $routeParams, $scope) {
			$scope.runnerId = $routeParams.runnerId;

			$scope.runnerData = {};
			$scope.detailedWeapon = {};
			$scope.addedWeapons = [];
			$scope.addingWeapons = false;
			$scope.allWeapons = [];
			$scope.detailedSpell = {};
			$scope.addedSpells = [];
			$scope.addingSpells = false;
			$scope.allSpells = [];

			function getRunner(runnerId) {
				var runnerDataUri = 'api/runners/' + runnerId;
				$http.get(runnerDataUri).then(function(response) {
					$scope.runnerData = response.data[0];

					var weaponIds = $scope.runnerData.weapons.map((weapon) => weapon.$id);
					var weaponData = {
						weaponIds: weaponIds
					};
					$http.post('api/weapons', weaponData).then(function(response) {
						$scope.weaponData = response.data;
					});

					var spellIds = $scope.runnerData.spells.map((spell) => spell.$id);
					var spellData = {
						spellIds: spellIds
					};
					$http.post('api/spells', spellData).then(function(response) {
						$scope.spellData = response.data;
					});
				});
			}

			getRunner($scope.runnerId);

			$scope.saveRunner = function() {
				delete $scope.runnerData._id;

				var data = {
					id: $scope.runnerId,
					runnerData: $scope.runnerData,
					addedWeapons: $scope.addedWeapons,
					addedSpells: $scope.addedSpells
				};

				$http.post('api/runner/edit', data).then(function(response) {
					$scope.addedWeapons = [];
					$scope.addedSpells = [];
					$scope.addingWeapons = false;
					$scope.addingSpells = false;
					getRunner($scope.runnerId);
				});
			};

			$scope.fetchWeapons = function() {
				$http.get('api/weapons/').then(function(response) {
					$scope.allWeapons = response.data;
					$scope.addingWeapons = true;
				});
			};

			$scope.addRunnerWeapon = function(clickEvent) {
				var weaponId = clickEvent.srcElement.name;
				$scope.addedWeapons.push(weaponId);
			};

			$scope.viewWeapon = function(index) {
				$scope.detailedWeapon = $scope.weaponData[index];
			};

			$scope.fetchSpells = function() {
				$http.get('api/spells/').then(function(response) {
					$scope.allSpells = response.data;
					$scope.addingSpells = true;
				});
			};

			$scope.addRunnerSpell = function(clickEvent) {
				var spellId = clickEvent.srcElement.name;
				$scope.addedSpells.push(spellId);
			};

			$scope.viewSpell = function(index) {
				$scope.detailedSpell = $scope.spellData[index];
			};
		}
	]
});
