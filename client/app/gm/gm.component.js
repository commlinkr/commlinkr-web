'use strict';

angular.module('gm').component('gm', {
	templateUrl: 'gm/gm.template.html',
	controller: [
		'$http',
		'$scope',
		function GmController($http, $scope) {
			$scope.initiatives = [];
			$scope.sceneId = '6000f81452b5e0d3a20e3b29';

			$http.get('api/initiatives/' + $scope.sceneId).then(function(response) {
				$scope.initiatives = response.data;
			});
		}
	]
});
