'use strict';

// Register `runnersList` component, along with its associated controller and template
angular.module('runnersList').component('runnersList', {
	templateUrl: 'runners-list/runners-list.template.html',
	controller: [
		'$http',
		'$scope',
		function RunnerListController($http, $scope) {
			$scope.roster = [];
			$http.get('api/runners').then(function(response) {
				$scope.roster = response.data;
			});
		}
	]
});
